<?
class ArduinoDhtSensor extends IPSModule {
	public function Create() {
		parent::Create();
		
		// Properties erstellen
		$this->RegisterPropertyInteger("BoardCode", 0);
		$this->RegisterPropertyInteger("BoardPin", 0);
		
		// Variablen erstellen
		$this->RegisterVariableFloat("TemperatureVariable", "Temperature", "~Temperature");
		$this->RegisterVariableInteger("HumidityVariable", "Humidity", "~Humidity");
		
		// Mit Bridge verbinden
		$this->ConnectParent("{036FBDE2-9FA3-45F0-8011-1DF98BE83169}");
	}
	public function ReceiveData($JSONString) {
		$data = json_decode($JSONString);
		$command = $data->Buffer;
		// Am Doppelpunkt wird das Command getrennt
		$parts = explode(":", $command);
		// Daten Parsen (nur wenn die Nachricht auch an diese Instant gerichtet war)
		if(intval($parts[0]) === $this->ReadPropertyInteger("BoardCode") && $parts[1] === "DHT" && intval($parts[2]) === $this->ReadPropertyInteger("BoardPin")) {
			SetValue($this->GetIDForIdent("TemperatureVariable"), floatval($parts[3]));
			SetValue($this->GetIDForIdent("HumidityVariable"), round(floatval($parts[4])));
		}
	}
}
?>