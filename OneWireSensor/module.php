<?
class ArduinoOneWireSensor extends IPSModule {
	public function Create() {
		parent::Create();
		
		// Properties erstellen
		$this->RegisterPropertyInteger("BoardCode", 0);
		$this->RegisterPropertyString("OneWireAddress", "0000000000000000");
		
		// Variablen erstellen
		$this->RegisterVariableFloat("TemperatureVariable", "Temperature", "~Temperature");
		
		// Mit Bridge verbinden
		$this->ConnectParent("{036FBDE2-9FA3-45F0-8011-1DF98BE83169}");
	}
	public function ReceiveData($JSONString) {
		$data = json_decode($JSONString);
		$command = $data->Buffer;
		// Am Doppelpunkt wird das Command getrennt
		$parts = explode(":", $command);
		if(intval($parts[0]) === $this->ReadPropertyInteger("BoardCode") && $parts[1] === "OneWire" && $parts[2] === $this->ReadPropertyString("OneWireAddress")) {
			SetValue($this->GetIDForIdent("TemperatureVariable"), floatval($parts[3]));
		}
	}
}
?>