<?
class ArduinoBoardBridge extends IPSModule {
	public function Create() {
		parent::Create();
		
		// Puffer
		$this->RegisterVariableString("ReceiveBuffer", "Buffer");
		
		// Mit ServerSocket verbinden
		$this->ConnectParent("{8062CF2B-600E-41D6-AD4B-1BA66C32D6ED}");
	}
	public function ReceiveData($JSONString) {
		$data = json_decode($JSONString);
		// Empfangenen Text anf�gen
		$bufid = $this->GetIDForIdent("ReceiveBuffer");
		$buffer = GetValue($bufid) . $data->Buffer;	
		// Am Semikolon wird getrennt
		$pos = strpos($buffer, ";");
		if($pos !== false) {
			$command = substr($buffer, 0, $pos);
			$buffer = substr($buffer, $pos + 1);
			// Daten an Ger�te senden
			$this->SendDataToChildren(json_encode(array("DataID" => "{1AC1E683-DE42-409B-818B-E13CC5B25D03}", "Buffer" => $command)));
		}
		SetValue($bufid, $buffer);
	}
}
?>