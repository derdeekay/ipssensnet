GUIDs:
	Library: 						{1BD7B7E7-BD8A-4F8C-96FB-8EA650329A67} // Bibliotheks-GUID
	Modul Arduino Board Bridge: 	{036FBDE2-9FA3-45F0-8011-1DF98BE83169} // Modul-GUID
	Modul Arduino Dht Sensor:		{529E22AE-D93E-402D-AE54-B070C977AD58} // Modul-GUID
	Modul Arduino OneWire Sensor:	{3E3BE893-A22D-4BEC-BDE5-656AFAC184F5} // Modul-GUID
	Interface Bridge->Sensor:		{1AC1E683-DE42-409B-818B-E13CC5B25D03} // Eigenes Interface, mit dem der Splitter Daten an die Ger�te �bergibt
	Interface Sensor->Bridge:		{2858C391-4561-4102-960D-F41FEA8B817D} // Eigenes Interface, mit dem die Ger�te Daten an den Splitter �bergeben
	IPS IO_RX:						{018EF6B5-AB94-40C6-AA53-46943E824ACF} // Empfangen von Daten von IO-Ger�ten